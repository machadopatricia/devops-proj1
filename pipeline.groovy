pipeline
{
    agent any
    options { timestamps () }

    environment
    {
        registry = "machadopatricia/devops-proj1"
        registryCredential = "dockerhub_id"
        dockerImage = ''
    }

    stages
    {
        stage('Clone Repository')
        {
            steps
            {
                git branch: "main", url: 'https://gitlab.com/machadopatricia/devops-proj1.git'
            }
        }
        stage('Build Docker Image')
        {
            steps
            {
                script
                {
                    dockerImage = docker.build registry + ":develop"
                }
            }
        }
        stage('Send Image to Docker Hub')
        {
            steps
            {
                script
                {
                    docker.withRegistry('', registryCredential)
                    {
                        dockerImage.push()
                    }
                }
            }
        }
        stage('Deploy')
        {
            steps
            {
                step([$class: 'AWSCodeDeployPublisher',
                applicationName: 'devops-proj1',
                awsAccessKey: 'AKIASKKGNQ5MVPFNWP2X',
                awsSecretKey: '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',
                credentials: 'awsAccessKey',
                deploymentGroupAppspec: false,
                deploymentGroupName: 'devops-proj1',
                deploymentMethod: 'deploy',
                excludes: '',
                iamRoleArn: '',
                includes: '**',
                pollingFreqSec: 15,
                pollingTimeOutSec: 600,
                proxyHost: '',
                proxyPort: 0,
                region: 'us-east-1',
                s3bucket: 'devops-proj1-pmac',
                s3prefix: '',
                subdirectory: '',
                versionFileName: '',
                waitForCompletion: true])
            }
        }
        stage('Cleaning up')
        {
            steps
            {
                sh "docker rmi $registry:develop"
            }
        }
    }
}